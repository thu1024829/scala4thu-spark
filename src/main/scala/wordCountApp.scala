import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac022 on 2017/5/8.
  */
object wordCountApp extends App{
  val conf = new SparkConf().setAppName("wordCount")
    .setMaster("local[*]")
  val sc =new SparkContext(conf)

  val lines=sc.textFile("/Users/mac022/Downloads/spark-doc.txt")
  //lines.take(10).foreach(println)
  //lines.flatMap(str=>str.split(" ").toList).take(10).foreach(println)
  val words=lines.flatMap(i=>i.split(" "))

  words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr) //可縮減成words.map(_->1).reduceByKey(_+_)
        //.groupBy(str=>str).mapValues(_.size)
    .take(10)
    .foreach(println)
  //List("Mark","Spark","RDD","RDD")
    //.groupBy(str=>str)
    //.mapValues(_.size)

  readLine()

}
