import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac022 on 2017/5/8.
  */
object FilterDemoApp  extends App{

  val conf = new SparkConf().setAppName("FilterDemo")
    .setMaster("local[*]")
  val sc =new SparkContext(conf)

  //odds
  sc.parallelize(1 to 100).filter(i=>i%2==1)
    .foreach(v=>println(v))

  //firstLetterIsM

  val char:Char='M'
  val str:String="M"
  sc.textFile("./names.txt")
      //.filter(name=>name.head=='M')
        .filter(name=>name.startsWith("M"))
    .foreach(println)
      //.foreach(name=>println(name(0),name(0),getClass))

}
