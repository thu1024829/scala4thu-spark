import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac022 on 2017/5/1.
  */
object SquareApp extends App{
  val conf=new SparkConf().setAppName("Square").setMaster("local[*]")

  val sc=new SparkContext(conf)

  val intRdd=sc.parallelize(1 to 100000)

  val squares=intRdd.map(x=>x*x)

  //squares.foreach(println)
  //squares.collect().foreach(println)

}
